const date = new Date();

dayMonth1 = [1, 4, 7, 10, 13, 16, 19, 22, 25, 28];//Дни месяца
dayMonth2 = [2, 5, 8, 11, 14, 17, 20, 23, 26, 29];
dayMonth3 = [3, 6, 9, 12, 15, 18, 21, 24, 27, 30];

timeTable1 = [0, 2, 6, 8, 12, 14, 18, 20];//Периоды отключения электроэнергии(0-2, 6-8 и т.д.)
timeTable2 = [4, 6, 10, 12, 16, 18, 22, 24];
timeTable3 = [2, 4, 8, 10, 14, 16, 20, 22];
timeTable = [timeTable1, timeTable2, timeTable3];

const day = date.getDate();
const hour = date.getHours();
const minute = date.getMinutes();
let leftHoursOff;//Осталось до отключения электроэнергии, часов\минут
let leftMinuteOff;
let leftHoursOn;//Осталось до включения электроэнергии, часов\минут
let leftMinuteOn;
let checkTimeTable;
let timeTablePointer;

checkTimeTable = dayMonth1.includes(day);

if (checkTimeTable == true) {
    timeTablePointer = 1
}

checkTimeTable = dayMonth2.includes(day);

if (checkTimeTable == true) {
    timeTablePointer = 2;
}

checkTimeTable = dayMonth3.includes(day);

if (checkTimeTable == true) {
    timeTablePointer = 3;
}

if (hour >= timeTable[timeTablePointer - 1][0] && hour < timeTable[timeTablePointer - 1][1] || hour >= timeTable[timeTablePointer - 1][2] && hour < timeTable[timeTablePointer - 1][3] || hour >= timeTable[timeTablePointer - 1][4] && hour < timeTable[timeTablePointer - 1][5] || hour >= timeTable[timeTablePointer - 1][6] && hour < timeTable[timeTablePointer - 1][7]) {
    document.write('Электроэнергия отключена');
    console.log('Электроэнергия отключена');

    for (j = 1; j < 8; j += 2) {
        if (hour < timeTable[timeTablePointer - 1][j]) {
            leftHoursOn = timeTable[timeTablePointer - 1][j] - hour - 1;
            leftMinuteOn = 60 - minute;
            document.write('До включения электроэнергии осталось ' + leftHoursOn + ' ч. ' + leftMinuteOn + ' мин.');
            console.log('До включения электроэнергии осталось ' + leftHoursOn + ' ч. ' + leftMinuteOn + ' мин.');
            break;
        }
    }
}

for (j = 0; j < 8; j += 2) {
    if (hour < timeTable[timeTablePointer - 1][j]) {
        leftHoursOff = timeTable[timeTablePointer - 1][j] - hour - 1;
        leftMinuteOff = 60 - minute;
        document.write('До следующего отключения электроэнергии осталось ' + leftHoursOff + ' ч. ' + leftMinuteOff + ' мин.');
        console.log('До следующего отключения электроэнергии осталось ' + leftHoursOff + ' ч. ' + leftMinuteOff + ' мин.');
        break
    }
}

if (leftHoursOff == undefined) {
    document.write('Следующее отключение электроэнергии будет завтра');
    console.log('Следующее отключение электроэнергии будет завтра');
}
